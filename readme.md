# Nvidia Optimus Guide for Debian
This is the official guide to setup the NVIDIA optimus card in Debian. This guide should work in Debian and Debian based distros like Ubuntu.

## Prerequisite
Make sure to add the contrib and non-free components to /etc/apt/sources.list:
```
sudo nano /etc/apt/sources.list
```

---
sources.list file:
\# See https://wiki.debian.org/SourcesList for more information.
deb http://deb.debian.org/debian testing main contrib non-free
deb-src http://deb.debian.org/debian testing main contrib non-free

---

## Uninstall Bumblee / Primus
Remove the bumblee an primus packages:
```
sudo apt remove primus libprimus-vk1 nvidia-primus-vk-common nvidia-primus-vk-wrapper primus-libs primus-nvidia primus-vk primus-vk-nvidia bumblebee bumblebee-nvidia
```

Just to be safe, purge the bumblee and primus files:
```
sudo apt purge primus libprimus-vk1 nvidia-primus-vk-common nvidia-primus-vk-wrapper primus-libs primus-nvidia primus-vk primus-vk-nvidia bumblebee bumblebee-nvidia
```

## Xorg X Server Intel Display Driver
Remove the outdated xserver-xorg-video-intel package:
```
sudo apt remove xserver-xorg-video-intel
```

## Nvidia Drivers
Install the Nvidia drivers:
```
sudo apt install nvidia-driver firmware-misc-nonfree
```

Reboot the system:
```
systenctl reboot
```

## Debugging Tools
Install the debuggging tools:
```
sudo apt install intel-gpu-tools nvidia-smi vulkan-tools
```

## Ensure the Drivers Works Part 1
Run the nvidia gpu monitor:
```
nvidia-smi -l
```

Run the vkvube program:
```
vkcube
```

## Ensure the Drivers Works Part 2
Run the intel gpu monitor:
```
sudo intel_gpu_top
```

Run the glxgears program:
```
glxgears
```

## References
[Debian Documentation - Using NVIDIA PRIME Render Offload](https://wiki.debian.org/NVIDIA%20Optimus#PRIMEOffload) 